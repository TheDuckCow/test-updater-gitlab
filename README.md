# Test Gitlab (Public) repository updating

Purpose: to test using the gitlab API to pull updates for a public repository. Counterpart will then be done for a private repo.

## Usage

Links to the API:
https://docs.gitlab.com/ee/api/README.html

Go to https://gitlab.com/profile/personal_access_tokens and generate a token, save this key for future api use (replacing the XXXX's below accordingly)

## Testing curl commands

First get all projects & ID's (confirmed working as expected)

```
curl -XGET --header "PRIVATE-TOKEN: XXXX" https://gitlab.com/api/v3/projects/owned
```

From here, you can look at the ID for any owned repositories. use that as the ID moving forward, in this case it's "3645616". You can also compare / get the project ID from https://gitlab.com/TheDuckCow/test-updater-gitlab/edit >> Project ID

Now get list of branches: (not confirmed working)

```
curl -XGET --header "PRIVATE-TOKEN: XXXX" https://gitlab.com/api/v3/projects/3645616/repository/branches

# response

[{
	"name": "master",
	"commit": {
		"id": "b0a3cf84d458d596e7d99987acc1f93c1d06fe53",
		"short_id": "b0a3cf84",
		"title": "updating readme with more progress",
		"created_at": "2017-07-05T17:47:26.000-04:00",
		"parent_ids": [
		  "2cbac1d3561df4f522264d59bc7e565425120dce"
		],
		"message": "updating readme with more progress ",
		"author_name": "TheDuckCow",
		"author_email": "theduckcow@live.com",
		"authored_date": "2017-07-05T17:47:26.000-04:00",
		"committer_name": "TheDuckCow",
		"committer_email": "theduckcow@live.com",
		"committed_date": "2017-07-05T17:47:26.000-04:00"
	},
	"merged": false,
	"protected": true,
	"developers_can_push": false,
	"developers_can_merge": false
}]
```


From the above, we can extract the last commit to the master branch is `b0a3cf84d458d596e7d99987acc1f93c1d06fe53` which we should be able to then directly download e.g. using wget or potentially also in a browser:

```
# download direct master only
wget --header "PRIVATE-TOKEN: XXXX" "https://gitlab.com/api/v3/projects/3645616/repository/archive.zip" # confirmed works, direct latest master can also opt to use tar.gz

# download specific commit
wget --header "PRIVATE-TOKEN: XXXX sha:b0a3cf84d458d596e7d99987acc1f93c1d06fe53" "https://gitlab.com/api/v3/projects/3645616/repository/archive.zip"

# download specific tag
# TBD

```

These all downlaod a file called archive.zip to the current working directory.

## Gitlab tags and releases, the process to be used by updaters

Assume we already have the project ID hard coded into the project, 3645616. Then, we could get a list of tags:

```
curl -XGET --header "PRIVATE-TOKEN: XXXX" https://gitlab.com/api/v3/projects/3645616/repository/tags

[{
	"name": "TestCreatingTag",
	"message": "Sample Message, tagged at master",
	"commit": {
	"id": "fa51b4783633e6b22f9060ad9056c118ece58ce7",
	"message": "fixed, had wrong project ID ",
	"parent_ids": [
	  "b0a3cf84d458d596e7d99987acc1f93c1d06fe53"
	],
	"authored_date": "2017-07-05T17:54:23.000-04:00",
	"author_name": "TheDuckCow",
	"author_email": "theduckcow@live.com",
	"committed_date": "2017-07-05T17:54:23.000-04:00",
	"committer_name": "TheDuckCow",
	"committer_email": "theduckcow@live.com"
	},
	"release": {
		"tag_name": "TestCreatingTag",
		"description": "Sample Release notes here, tagged at master."
	}
},
{
	"name": "TagRemovedPY",
	"message": "Message notes: Removed PY file",
	"commit": {
	"id": "5c3a5ee27202d5a4983e7fcd83bdf532dcc1f59a",
	"message": "removing py file, not in use ",
	"parent_ids": [
	  "fa51b4783633e6b22f9060ad9056c118ece58ce7"
	],
	"authored_date": "2017-07-05T18:20:17.000-04:00",
	"author_name": "TheDuckCow",
	"author_email": "theduckcow@live.com",
	"committed_date": "2017-07-05T18:20:17.000-04:00",
	"committer_name": "TheDuckCow",
	"committer_email": "theduckcow@live.com"
	},
	"release": {
		"tag_name": "TagRemovedPY",
		"description": "Release notes: removed PY file for this tag."
	}
}]

```

From this list of tags, if needed order by the date, and get reference to lastest on a given branch (default to master); we extract the sha from the response provided inline for each tag in the above command via tag.commit.id

```
# command line download for specific repo
wget --header "PRIVATE-TOKEN: XXXX sha:b0a3cf84d458d596e7d99987acc1f93c1d06fe53" "https://gitlab.com/api/v3/projects/3645616/repository/archive.zip"

# or via direct url for url-downloading e.g. in browser
https://gitlab.com/api/v3/projects/3645616/repository/archive.zip?sha=TestCreatingTag

wget --header "PRIVATE-TOKEN: XXXX" https://gitlab.com/api/v3/projects/3645616/repository/archive.zip?sha=TestCreatingTag

https://gitlab.com/api/v3/projects/3645616/repository/archive.zip?sha=devtest

```

To create tags on gitlab, go to:
https://gitlab.com/TheDuckCow/test-updater-gitlab/tags/new

Tags can also be created by git itself


### Further examples and links
https://docs.gitlab.com/ee/api/commits.html

https://docs.gitlab.com/ce/api/session.html

